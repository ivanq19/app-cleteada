import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl} from '@angular/forms';
import { UploadService } from '../services/upload.service';//connetion with the server

//To change the language of the calendar, library ngx-bootstrap
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
defineLocale('es', esLocale);

//Working with Maps, Libraries ngx-OpenLayer, ol and @types/ol
import Map from 'ol/Map';
import OSM from 'ol/source/OSM';
import View from 'ol/View';
import {fromLonLat} from 'ol/proj.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import Point from 'ol/geom/Point';
import VectorSource from 'ol/source/Vector.js';
import {Icon, Style} from 'ol/style';
import Feature from 'ol/Feature';

/*
https://www.techiediaries.com/angular-by-example-httpclient-get/
*/

@Component({
  selector: 'app-historic',
  templateUrl: './historic.component.html',
  styleUrls: ['./historic.component.css']
})


export class HistoricComponent implements OnInit {
  signupForm: FormGroup; //To create json with form data
  layers = [];

  map;
  longitude = -84.2116318; 
  latitude = 10.0162497;
  alajuela;     //first point drawn on the map, this will change when someone clicks on the map
  vectorSource;
  vectorLayer;
  rasterLayer;

  

  constructor(private _builder: FormBuilder,private bsLocaleService: BsLocaleService,
    private serviceUpload: UploadService) { 
    this.signupForm = this._builder.group({
      accident: [false],
      stole: [false],
      heatMap: new FormControl({value: '', disabled: true}),
      date: [null],
    });

    //It is used to change the language to the calendar
    this.bsLocaleService.use('es');
  }

  send(values){
    //This works because there are only two accidental or stolen layers
    if(this.layers.length >= 1){
      this.map.removeLayer(this.layers[0]);
      this.layers.pop();
    }

    //show layers
    this.showDataOnMap(values);
  }

  //Show layers on the map
  showDataOnMap(data){
    if(data.accident == true && data.stole == true){
      if(data.date !== null){
        var startDate = this.changeDateFormat(data.date[0]);
        var endDate = this.changeDateFormat(data.date[1]);
        this.getReportsByDateRange(startDate,endDate);
      }else{
        this.getReportData();}

    }else{
      if(data.accident == true){
        if(data.date !== null){
          var startDate = this.changeDateFormat(data.date[0]);
          var endDate = this.changeDateFormat(data.date[1]);
          this.getReportsByDateRangeAndType(startDate,endDate, 'accidente');
        }else{
          this.getAccidentReport('accidente');}
      }
  
      if(data.stole == true){
        if(data.date !== null){
          var startDate = this.changeDateFormat(data.date[0]);
          var endDate = this.changeDateFormat(data.date[1]);
          this.getReportsByDateRangeAndType(startDate,endDate, 'robo');
        }else{
          this.getAccidentReport('robo');}
      }
    }


  }

  //GET accidents and robberies
  getReportData(){
    this.serviceUpload.getReport().subscribe((reports: any[])=>{
      this.parseJson(reports,'data');
    });  
  }

  //GET accidents or robberies
  getAccidentReport(type){
    this.serviceUpload.getAccidentReport(type).subscribe((reports: any[])=>{
      this.parseJson(reports,'data');
    }); 
  }

  //GET accidents and robberies by date
  getReportsByDateRange(startDate,endDate){
    this.serviceUpload.getReportsByDate(startDate,endDate).subscribe((reports: any[])=>{
      this.parseJson(reports,'data');
    }); 
  }

  //GET accidents or robberies by date
  getReportsByDateRangeAndType(startDate,endDate, type){
    this.serviceUpload.getReportsByDateAndType(startDate,endDate, type).subscribe((reports: any[])=>{
      this.parseJson(reports,'data');
    }); 
  }

  //Parser json
  parseJson(json,key){
    var reportArray = json[key];
    var point = [];
    for(var index in reportArray){

      var coordinate = reportArray[index].location
      var resultArray = coordinate.split(',');

      var longitude = resultArray[0];
      var latitude = resultArray[1];
  
      if((reportArray[index].type).localeCompare("accidente")){
        point.push(this.createPoint(longitude,latitude,'assets/point3.png'));
      }else{
        point.push(this.createPoint(longitude,latitude,'assets/point4.png'));
      }
      
    } 
    this.drawPoints(point);
  }

  //Create a point in the map
  createPoint(longitude, latitude, image){
    this.alajuela = new Feature({
      geometry: new Point(fromLonLat([longitude,latitude]))
    });

    this.alajuela.setStyle(new Style({
      image: new Icon({
        crossOrigin: 'anonymous',
        src: image,
      })
    }));

    return this.alajuela;
  }

  //Draw a point in the map
  drawPoints(vector){
    this.vectorLayer = new VectorLayer({
      source: new VectorSource({
        features: vector
      })
    });
    
    //add the new layer
    this.map.addLayer(this.vectorLayer);
    this.layers.push(this.vectorLayer);
    
  }

  //This function performs the initial configuration to show the map
  initializeMap(){
    this.rasterLayer = new TileLayer({
      source: new OSM()
    });

    this.map = new Map({
      target: 'map',
      //controls: [],
      //interactions: [],
      layers: [ this.rasterLayer],
      view: new View({
        center: fromLonLat([this.longitude, this.latitude]),
        zoom: 14,
      })
    });
  }

  //change the date format received
  changeDateFormat(dateFormat) {
    var date = new Date(dateFormat),
    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
    day = ("0" + date.getDate()).slice(-2);
    //return [day, mnth, date.getFullYear()].join("-");
    return [date.getFullYear(), mnth,day].join("-");
  }

  ngOnInit() {
    this.initializeMap();
  }

}
