import { Injectable } from '@angular/core';
import { HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

/*
https://www.techiediaries.com/angular-http-client/
https://www.youtube.com/watch?v=rdLJNGZvlAA
*/
export class UploadService {
  
  constructor(private http: HttpClient) { }

  uploadImage(formData) {
    let urlAPI = 'http://localhost:3000/api/upload-image';
    return this.http.post(urlAPI, formData);
  }

  uploadReport(reportData) {
    let urlAPI = 'http://localhost:3000/api/Reports';
    return this.http.post(urlAPI, reportData);
  }

  uploadGpx(gpxFile) {
    let urlAPI = 'http://localhost:3000/api/upload-gpx';
    return this.http.post(urlAPI, gpxFile);
  }

  getReport() {
    let urlAPI = 'http://localhost:3000/api/Reports';
    return this.http.get(urlAPI);
  }

  getAccidentReport(type) {
    let urlAPI = 'http://localhost:3000/api/Reports';
    const url = `${urlAPI}/${type}`;
    return this.http.get(url);
  }

  getReportsByDate(startDate, endDate){
    let urlAPI = 'http://localhost:3000/api/Reports';
    const url = `${urlAPI}/${startDate}/${endDate}`;
    return this.http.get(url);
  }

  getReportsByDateAndType(startDate, endDate, type){
    let urlAPI = 'http://localhost:3000/api/Reports';
    const url = `${urlAPI}/${startDate}/${endDate}/${type}`;
    return this.http.get(url);
  }


  
}
