export class Report {
    id?: string;
    name: string;
    email: string;
    description: string;
    date: string;
    location: string;
    type: string;
    image: string;
    n?: number;
}
