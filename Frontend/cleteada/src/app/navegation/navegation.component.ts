import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder} from '@angular/forms';
import { UploadService } from '../services/upload.service';//connetion with the server
import { ToastrService } from 'ngx-toastr';

import { Route } from '../models/route'

//Reference https://jsfiddle.net/jonataswalker/079xha47/

//Working with Maps, Libraries ngx-OpenLayer, ol and @types/ol
import Map from 'ol/Map';
import OSM from 'ol/source/OSM';
import View from 'ol/View';
import {fromLonLat, transform} from 'ol/proj.js';
import {Tile as TileLayer, Vector as VectorLayer, Vector} from 'ol/layer';
import Point from 'ol/geom/Point';
import VectorSource from 'ol/source/Vector.js';
import {Icon, Style} from 'ol/style';
import Feature from 'ol/Feature';
import GPX from 'ol/format/GPX';
import {Stroke} from 'ol/style';
import Polyline from 'ol/format/Polyline';



@Component({
  selector: 'app-navegation',
  templateUrl: './navegation.component.html',
  styleUrls: ['./navegation.component.css']
})

export class NavegationComponent implements OnInit {
  selectedFiles: string | ArrayBuffer; //send file to server 
  signupForm: FormGroup; //To create json with form data

  map;
  longitude = -84.2116318; 
  latitude = 10.0162497;
  alajuela;     //first point drawn on the map, this will change when someone clicks on the map
  rasterLayer;

  //object route
  selectedRoute: Route = new Route();

  constructor(private _builder: FormBuilder,
    private serviceUpload: UploadService, private toastr: ToastrService) { 
    this.signupForm = this._builder.group({
      hill: [false],
      traffic: [false],
      shading: [false],
    });
  }

  //set data request for route
  setDataRequest(data){
    this.selectedRoute.hill = data.hill;
    this.selectedRoute.traffic = data.traffic;
    this.selectedRoute.shading = data.shading;
  }

  //send data request to server
  sendForm(values){
    //NOT implented
    this.toastr.info('Agrega un punto de salida','Ruta');
    this.setDataRequest(values);
    console.log(this.selectedRoute)
  }

  //show gpx on the map
  setGpxFile(gpx){
    var vector = new VectorLayer({
      source: new VectorSource({
        url: gpx,
        format: new GPX()
      }),
      style: new Style({//trace style
        stroke: new Stroke({
          color: '#ff004c',
          width: 3
        })
      }),
    });
    this.map.addLayer(vector);//add vector
  }

  //mensssage of success
  showToasterSuccess(){
    this.toastr.success('Cargado con éxito!','GPX');
  }
  
  //mensssage of success
  showToasterWarning(message){
    this.toastr.warning(message,'Error!');
  }

  //load gpx file
  onSelectFile(event) { 
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      var name = event.target.files[0].name
      if (name.indexOf(".gpx") != -1){
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event) => { // called once readAsDataURL is completed   
          this.selectedFiles = reader.result; 
          this.setGpxFile(this.selectedFiles); 
          this.showToasterSuccess();
        }
      }else{
        this.showToasterWarning('Debe ser un archivo gpx!');
      }
    }else{
      this.showToasterWarning('El archivo no se cargo correctamente!');
    }

  }


  //This function performs the initial configuration to show the map
  initializeMap(self){
    var points = [],
    //http://project-osrm.org/docs/v5.5.1/api/#general-options
    url_osrm_nearest = '//router.project-osrm.org/nearest/v1/driving/',
    url_osrm_route = '//router.project-osrm.org/route/v1/driving/',
    vectorSource = new VectorSource(),
    vectorLayer = new VectorLayer({
      source: vectorSource
    }),
    styles = {
      route: new Style({
          stroke: new Stroke({
          width: 6, color: '#09a502',
        })
      }),
      icon: new Style({
        image: new Icon({
          crossOrigin: 'anonymous',
          src: 'assets/point4.png',
        })
      })
    };

    this.rasterLayer = new TileLayer({
      source: new OSM()//raster OpenStreetMap
    });

    this.map = new Map({
      target: 'map',
      layers: [ this.rasterLayer, vectorLayer],
      view: new View({
        center: fromLonLat([this.longitude, this.latitude]),
        zoom: 14,
      })
    });


    this.map.on('click', function(evt){
      utils.getNearest(evt.coordinate).then(function(coord_street){
        var last_point = points[points.length - 1];
        var points_length = points.push(coord_street);
    
        //add marker in the map
        utils.createFeature(coord_street);
    
        if (points_length < 2) {
          //console.log("Falta un punto mas");
          self.toastr.info('Agrega el punto de destino','Ruta');
          return;
        }
    
        //get the route
        var point1 = last_point.join();
        var point2 = coord_street;
        
        fetch(url_osrm_route + point1 + ';' + point2).then(function(r) { 
          return r.json();
        }).then(function(json) {
          console.log(json.waypoints)
          if(json.code !== 'Ok') {
            //console.log("No funciona")
            return;
          }
          //console.log("Funciona")
          //points.length = 0;
          utils.createRoute(json.routes[0].geometry);
        });
      });
    });
    
    var utils = {
      getNearest: function(coord){
        var coord4326 = utils.to4326(coord);    
        return new Promise(function(resolve, reject) {
          //make sure the coord is on street
          fetch(url_osrm_nearest + coord4326.join()).then(function(response) { 
            // Convert to JSON
            return response.json();
          }).then(function(json) {
            if (json.code === 'Ok') resolve(json.waypoints[0].location);
            else reject();
          });
        });
      },
      createFeature: function(coord) {
        var feature = new Feature({
          type: 'place',
          geometry: new Point(fromLonLat(coord))
        });
        feature.setStyle(styles.icon);
        vectorSource.addFeature(feature);
      },
      createRoute: function(polyline) {
        var route = new Polyline({
          factor: 1e5
        }).readGeometry(polyline, {
          dataProjection: 'EPSG:4326',
          featureProjection: 'EPSG:3857'
        });
        var feature = new Feature({
          type: 'route',
          geometry: route
        });
        feature.setStyle(styles.route);
        vectorSource.addFeature(feature);
      },
      to4326: function(coord) {
        return transform([
          parseFloat(coord[0]), parseFloat(coord[1])
        ], 'EPSG:3857', 'EPSG:4326');
      }
  }}

  

  ngOnInit() {
    this.initializeMap(this);
  }

}
