import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { UploadService } from '../services/upload.service';//connetion with the server

//To change the language of the calendar, library ngx-bootstrap
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
defineLocale('es', esLocale);

//Working with Maps, Libraries ngx-OpenLayer, ol and @types/ol
import Map from 'ol/Map';
import OSM from 'ol/source/OSM';
import View from 'ol/View';
import {fromLonLat, transform} from 'ol/proj.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import Point from 'ol/geom/Point';
import VectorSource from 'ol/source/Vector.js';
import {Icon, Style} from 'ol/style';
import Feature from 'ol/Feature';
import { ToastrService } from 'ngx-toastr';


//import model class
import {Report} from '../models/report';

/*
https://openlayers.org/en/latest/doc/tutorials/bundle.html
https://stackblitz.com/edit/angular-osm
https://openlayers.org/en/latest/examples/
https://openlayers.org/en/latest/examples/mouse-position.html
https://openlayers.org/en/latest/examples/icon-color.html
https://gis.stackexchange.com/questions/174257/how-to-add-marker-dynamically-when-clicked-on-map-using-openlayers-3
https://openlayers.org/en/latest/apidoc/module-ol_coordinate.html
*/

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})



export class ReportsComponent implements OnInit {


  signupForm: FormGroup; //To create json with form data
  url: string | ArrayBuffer; //show image
  uploadedFiles: Array < File >; //send file to server

  map;
  longitude = -84.2116318; 
  latitude = 10.0162497;
  alajuela;     //first point drawn on the map, this will change when someone clicks on the map
  vectorSource;
  vectorLayer;
  rasterLayer;
  selectedReport: Report = new Report();
  

  constructor(private _builder: FormBuilder,private serviceUpload: UploadService, 
    private bsLocaleService: BsLocaleService, private toastr: ToastrService) {
    //generate json with the data of the form
    this.signupForm = this._builder.group({
      name: ['', Validators.required],
      email: ['', Validators.compose([Validators.email,Validators.required])],
      description: ['', Validators.required],
      accident: [false],
      stole: [false],
      date: [''],
      image:[''],
    });

    //It is used to change the language to the calendar
    this.bsLocaleService.use('es');
  }

  //mensssage of success
  showToasterSuccess(){
    this.toastr.success('Enviado con éxito!','Reporte!');
  }

   //mensssage of success
  showToasterWarning(){
    this.toastr.warning('Selecciona accidente o robo, pero no ambos!','Error!');
  }

  //Prepare information received to send to the server
  setDataReport(values){
    this.selectedReport.name = values.name;
    this.selectedReport.email = values.email;
    this.selectedReport.description = values.description;
    this.selectedReport.date = this.changeDateFormat(values.date);
    this.selectedReport.location = (this.longitude + ',' + this.latitude);
  
    if (values.accident == true){
      this.selectedReport.type = "accidente";
    }
    if (values.stole == true){
      this.selectedReport.type = "robo";
    }

    this.selectedReport.image =  this.getImageName(values.image); 
   
  }

  //button Enviar 
  send(values){
    this.setDataReport(values);

    if(values.accident == true && values.stole == true){
      this.showToasterWarning();

    }else{
      //console.log(this.selectedReport);
      //this.uploadImageToServer();
      this.uploadReportDataToServer();
    }
  }

  //show image in container
  onSelectFile(event) { 
    if (event.target.files && event.target.files[0]) {
      this.uploadedFiles = event.target.files;

      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event) => { // called once readAsDataURL is completed   
        this.url = reader.result;    
      }
    }
  }

  //upload image to server
  uploadImageToServer() {
    let formData = new FormData();
    for (var i = 0; i < this.uploadedFiles.length; i++) {
      formData.append("image", this.uploadedFiles[i], this.uploadedFiles[i].name);
    }
    this.serviceUpload.uploadImage(formData).subscribe((res)=> {
      console.log('response received is ', res);
    });
  }

  //upload report data to server
  uploadReportDataToServer(){
    this.serviceUpload.uploadReport(this.selectedReport).subscribe((res)=>{
      this.showToasterSuccess();
    });
  }

  //Return the image name, receives the image path
  getImageName(imagePath){
    var resultArray = imagePath.split('\\');
    var lenResultArray = resultArray.length;
    var imageName = resultArray[lenResultArray-1];

    return imageName;
  }

  //change the date format received
  changeDateFormat(dateFormat) {
    var date = new Date(dateFormat),
    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
    day = ("0" + date.getDate()).slice(-2);
    //return [day, mnth, date.getFullYear()].join("-");
    return [date.getFullYear(), mnth,day].join("-");
  }

  
  //Draw a point in the map
  drawPoint(longitude, latitude){
      this.alajuela = new Feature({
        geometry: new Point(fromLonLat([longitude,latitude]))
      });

      this.alajuela.setStyle(new Style({
        image: new Icon({
          crossOrigin: 'anonymous',
          src: 'assets/point2.png',
        })
      }));

      this.vectorLayer = new VectorLayer({
        source: new VectorSource({
          features: [this.alajuela]
        })
      });
      
      //add the new layer
      this.map.addLayer(this.vectorLayer);
  }
  
  //Get click position, show the coordinates, delete old point and add new point 
  clickOnMap(self){
    var cont = 1;
    self.map.on('singleclick', function(evt) {
      var coordinate = transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');
      self.longitude = coordinate[0];
      self.latitude = coordinate[1];

      //console.log(this.longitude,this.latitude)
      document.getElementById('longitude').innerHTML = self.longitude;
      document.getElementById('latitude').innerHTML = self.latitude;
      
      //delete feature, marker or point
      self.map.getLayers().item(cont).getSource().clear();
      cont++;
 
      // function drawPoint
      self.drawPoint(self.longitude,self.latitude)
    }); 
  }

  //This function performs the initial configuration to show the map
  initializeMap(){
    this.rasterLayer = new TileLayer({
      source: new OSM()
    });

    this.map = new Map({
      target: 'map',
      //controls: [],
      //interactions: [],
      layers: [ this.rasterLayer],
      view: new View({
        center: fromLonLat([this.longitude, this.latitude]),
        zoom: 15,
      })
    });

    //add point
    this.drawPoint(this.longitude,this.latitude);
    
    //function to show map point
    this.clickOnMap(this);
  }

  //The code here is executed when the page loads
  ngOnInit() {
    this.initializeMap()
  }
}