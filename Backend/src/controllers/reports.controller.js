import Report from '../models/Report';
import {Op} from 'sequelize';

//This function creates a new report 
export async function createReport(req,res){
    //get data
    const {name, email, description, date, location, type, image} = req.body;
    try {
        let newReport = await Report.create({
            name,
            email,
            description,
            date,
            location,
            type,
            image,
        }, {
            fields: ['name', 'email', 'description', 'date', 'location', 'type', 'image'],
        });
        
        if(newReport){
            //console.log(newReport);
           res.json({
                
                message: "Report create successfully",
                data: newReport
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something goes wrong',
            data: {}
        });
    }
}

//This funtion gets every reports 
export async function getReports(req,res){
    const reports = await Report.findAll();
    res.json({
        data: reports
    })
}

//This funtion gets every report of accident or stole type 
export async function getOneReport(req,res){
    const {type} =  req.params;
    const report = await Report.findAll({
        where:{
            type
        }
    });
    res.json({
        data: report
    })
}

//select * from reports where date between '2019-10-01' and '2019-10-19';
export async function getReportByDate(req,res){
    const {startDate} =  req.params;
    const {endDate} =  req.params;
    const report = await Report.findAll({
        where:{
            date:{
                [Op.between]: [startDate, endDate],
            },
        },
    });
    res.json({
        data: report
    })
}

//select * from reports where date between '2019-10-01' and '2019-10-19' and type='accidente'; 
export async function getReportByDateAndType(req,res){
    const {startDate} =  req.params;
    const {endDate} =  req.params;
    const {type} =  req.params;

    const report = await Report.findAll({
        where:{
            [Op.and]: [{type: type},{date:{[Op.between]: [startDate, endDate]}}], 
        },
    });
    res.json({
        data: report
    })
}
/*********************************************************************************** */
//This funtion deletes a report by id
export async function deleteReport(req,res){
    const {id} = req.params;
    try {
        const deleteRowCount = await Report.destroy({
            where:{
                id
            }
        });
    
        if(deleteRowCount){
            res.json({
                message: 'Report delete successfully',
                count: deleteRowCount
            });
           
        }
        
    } catch (error) {
        //console.log(error);
        res.status(500).json({
            message: 'Something goes wrong',
            count: 0
        });
    }
}

//This funtion updates a report by id
export async function updateReport(req,res){
    const {id} = req.params;
    const {name, email, description, date, location, type, image} = req.body;
    try {
        const reports = await Report.findAll({
            attributes: ['name', 'email', 'description', 'date', 'location', 'type', 'image'],
            where:{
                id
            }
        });

        if(reports.length > 0){
            reports.forEach(async report => {
                await report.update({
                    name,
                    email,
                    description,
                    date,
                    location,
                    type,
                    image,
                });
            });
        }
    
      
        res.json({
            message: 'Report update successfully',
            data: Reports
        });
        
    } catch (error) {
        //console.log(error);
        res.status(500).json({
            message: 'Something goes wrong',
            data: {}
        });
    }
}