import {database} from '../database/appCleteada.js'; 

//Function to test connection 
export async function testConnection(){
    try {
        database.authenticate()
                .then(()=>{
                    console.log('Connection has been established succefully.'); 
                })
                .catch(err =>{
                    console.error('Unable to connect to the database:', err);
        });
    } catch (err){
        console.error('Error testing connection ', err);
    }
}


// Function to insert new file 
export async function createGpxfile(fileName, filePath){
    const tableName = "gpxfiles";
    const tableAttributes = " (name, filepath)";
    const values = "('" + fileName + "', '" + filePath + "')"; 
    try {
        database.query("INSERT INTO " + tableName + tableAttributes + " VALUES " + values)
                .then(([results, metadata]) => {
                    console.log('Insertion results');
                    //console.log('results = ', results)
                    console.log('Modified elements ', metadata);
                })

    } catch (error) {
        console.error('Error creating = ', error);
    }
} 

// Function to get all the files fron DB
export async function getFiles(name, sunny, trafic, hills){ // was async 
    const tableName = "gpxfiles";
    var resultInfo; //not in use
    try {
        database.query("SELECT * FROM " + tableName + " WHERE name = '" + name + ".gpx';")
                .then(([results, metadata]) => {
                    console.log('Files result');
                    //console.log('results = ', results) //here comes the result form the query
                    console.log('Modified elements ', metadata.rowCount);

                    console.log(results.length)
                    var i;
                    for(i = 0; i < results.length; i++){

                        console.log(results[i].id)
                        console.log(results[i].name)
                        resultInfo.push(results[i])
                        //console.log(resultInfo)
                    }
                    // Return a list o elements 
                });
                // console.log(resultInfo)
    } catch (error) {
        console.error('Error consulting = ', error);
    }
    // Return a list o elements 
    return resultInfo;
    // console.log(resultInfo)
}