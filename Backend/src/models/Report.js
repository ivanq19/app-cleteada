import Sequelize from 'sequelize';
import {sequelize} from '../database/database';
export const  Op = Sequelize.Op;

// To use sequelize, here it is created the model of the reports table
// To more about sequelize you can see https://sequelize.org/master/manual/getting-started.html
const Report = sequelize.define('reports',{
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true
    },

    name:{
        type: Sequelize.TEXT
    },

    email:{
        type: Sequelize.TEXT
    },

    description:{
        type: Sequelize.TEXT
    },

    date:{
        type: Sequelize.DATE
    },

    location:{
        type: Sequelize.TEXT
    },

    type:{
        type: Sequelize.TEXT
    },

    image:{
        type: Sequelize.TEXT
    },

}, {
    timestamps: false  //To not have problems with the date
});

export default Report;