import Sequelize from 'sequelize';

export const database = new Sequelize(
    'appcleteada',
    'postgres',
    'ivanquesada',
    {
        port: 5432,
        host: '127.0.0.1',
        dialect: 'postgres',

        pool:{
            max: 10,  //max the connection
            min: 0,   //min the connection
            require: 3000,
            idle: 10000
        },

        logging: false //To not show logs in terminal
    }
)