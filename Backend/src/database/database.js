import Sequelize from 'sequelize';

export const sequelize = new Sequelize(
    'App-Cleteada', //name of the database
    'postgres',     //user 
    'root',         //password
    {   
        port: 5433, 
        host: '127.0.0.1',
        dialect: 'postgres',

        pool:{
            max: 10,  //max the connection
            min: 0,   //min the connection
            require: 3000,
            idle: 10000
        },

        logging: false //To not show logs in terminal
    }

)