
DROP DATABASE if exists appCleteada;

DROP USER cleteros;

CREATE USER cleteros WITH password 'bicicleta123';

CREATE DATABASE appCleteada WITH OWNER cleteros;


---Create tables
CREATE TABLE IF NOT EXISTS reports(
    id serial PRIMARY KEY,
    name text,
    email text,
    description text,
    date date NOT NULL,
    location text,
    type text,
    image text
);


CREATE TABLE gpxFiles(
	ID SERIAL NOT NULL,
	NAME TEXT NOT NULL,
	FILEPATH TEXT NOT NULL,
	UPLOADDATE TIMESTAMP NOT NULL DEFAULT NOW(), 
	PRIMARY KEY (ID)
);

---Insert reports 
INSERT into reports(name, email, description, date, location, type, image)
    values('Lorenzo Méndez R','lamrodriguez19@gmail.com', 
    'Este accidente me occurió por la sede InterUniversitaria', 
    '2019-10-08','-84.2116318,10.0162497' ,'accidente','imagen');
