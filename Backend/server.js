const express = require('express');  
const cors = require('cors');
const app = express();  
const {Pool, Client} = require('pg');
const async = require('async');
var fs = require('fs');
var path = require('path');

const PORT = 3000;   //port of connection

app.use(cors());

const bodyParser = require('body-parser');  


//Pool conects to the postgres data base
const pool = Pool({
    user: 'cleteros',
    host: '127.0.0.1',
    database: 'appcleteada',
    password: 'bicicleta123',
    port: 5432,
});

app.use(bodyParser.json());  
app.use(bodyParser.urlencoded({  
    extended: true
}));

//pool.query('SELECT NOW()', (err, res) => {
//    console.log(res)
//    pool.end()
//});


//function to make a query to the data base 
function sqlQuery (query, callback) {

    pool.connect(function(err, client, done){
        if(err){
            console.error('Error conecting to the DB' + err.stack);
            callback(err);
        } else {
            console.log('Connection established with DB');

            client.query(query, (err, res) => {

                if(err){
                    console.error('Error executing Query' + err.stack);
                    callback(err);
                } else {
                    console.log('Got Query results: ' + res.rows.leght);

                    async.each(res.rows, function(empRecord) {   
                        console.log(empRecord.name);
                    });
                }
                client.release();
            });
        }
    });
}
//var callB;
//console.log('Ejecutando la función sqlQuery');
//sqlQuery("SELECT NOW()", callB);
//console.log(callB);

//to start the server.js 
//if you want to start the server you can it with (nodemon server.js) in terminal
app.listen(PORT, () => console.log(`Server listening on PORT ${PORT}!`))